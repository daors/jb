﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using jb.Db;
using jb.Interfaces;
using jb.Models;
using jb.Repositories;
using jb.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace jb
{
    public class Startup
    {

        /* 
           This instance 'Configuration' contains all the information that was already read out by Program.cs class, 
           we can use then Configuration for the connection string when setting up the EF Core 
        */
        public IConfiguration Configuration { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {


            // Add support for database
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            // Add Identity adds cookie based authentication
            // Adds scoped classes for things like UserManager, SignInManager, PasswordHashers
            // NOTE: Automatically adds the validated user from a cookie to the HttpContext.User
            services.AddIdentity<JBanesaUser, IdentityRole>()
                // Adds a provider that generates unique keys and hashes for things like 
                // forgot password links, phone number verification codes etc...
                .AddDefaultTokenProviders()
                // Adds UserStore and RoleStore from this context 
                // That are consumed by the UserManager and RoleManager
                .AddEntityFrameworkStores<AppDbContext>();



            // Change password policy
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.SignIn.RequireConfirmedEmail = true;
            });


            // Here we register(add) our own services in dependency injection container
            services.AddTransient<IJBanesaUserRepository, JBanesaUserRepository>();
            services.AddTransient<IApartmentRepository, ApartmentRepository>();
            services.AddTransient<ICityRepository, CityRepository>();
            services.AddTransient<IUploadFileServices, UploadFilesService>();
            services.AddTransient<IBuildingRepository, BuildingRepository>();

            //Add EmailSender as a transient service.
            services.AddTransient<IEmailSender, EmailSender>();            

            //Register the AuthMessageSenderOptions configuration instance.
            services.Configure<AuthMessageSenderOptions>(Configuration);

            // Allow mvc in the application
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // the sequence on which we add the middleware component is important

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }            

            app.UseStatusCodePages();        

            // Serve static files 
            app.UseStaticFiles();

            // Setup Identity
            app.UseAuthentication();

            // setup MVC routes
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
