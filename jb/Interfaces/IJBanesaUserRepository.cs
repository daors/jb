﻿using jb.Models;
using jb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Interfaces
{
    public interface IJBanesaUserRepository
    {
        JBanesaUser GetJBanesaUser(JBanesaUser jbu);

        IEnumerable<Building> GetPostedBuildingsOfAJBanesaUserByUsername(string username);
        IEnumerable<Building> GetPostedBuildingsOfAJBanesaUserWithAddressByUsername(string username);

        IEnumerable<MatchmakingBuilding> GetMatchmakingBuildingsOfAJBanesaUserByUsername(string username);
        IEnumerable<MatchmakingBuilding> GetMatchmakingBuildingsOfAJBanesaUserWithAddressByUsername(string username);


        // needs to be replaced with the one that returns photos too
        IEnumerable<Building> GetFavouritedBuildingsOfAJBanesaUserByUsername(string username);

        List<BuildingViewModel> GetPostedBuildingsOfAJBanesaUserWithAddressByUsernameWithPhotos(string username);


        JBanesaUser GetById(string id);
        JBanesaUser GetByUserName(string username);
        JBanesaUser GetByUserNameWithEverything(string username);

        void SaveChanges();
    }
}
