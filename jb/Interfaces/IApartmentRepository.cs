﻿using jb.Models;
using jb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Interfaces
{
    public interface IApartmentRepository
    {
        // needs to be replaced with the one that returns photos too
        Apartment GetById(long id);

        // needs to be replaced with the one that returns photos too
        Apartment GetByIdIncludingUser(long id);

        // needs to be replaced with the one that returns photos too
        Apartment GetByIdIncludingUserAndCity(long id);

        // needs to be replaced with the one that returns photos too
        IEnumerable<Apartment> GetAll();
        
        // needs to be replaced with the one that returns photos too
        IEnumerable<Apartment> GetAllIncludingCity();

        IEnumerable<Apartment> GetAllIncludingUserAndCity();

        IEnumerable<Apartment> GetAllForSaleIncludingUserAndCity();
        IEnumerable<Apartment> GetAllForRentIncludingUserAndCity();

        List<ApartmentViewModel> GetAllIncludingUserAndCityWithPhotos();

        List<ApartmentViewModel> GetAllForSaleIncludingUserAndCityWithPhotos();

        List<ApartmentViewModel> GetAllForRentIncludingUserAndCityWithPhotos();

        ApartmentViewModel GetByIdIncludingUserAndCityWithPhotos(long Id);

        void Add(Apartment apartment);

        void Add(Apartment apartment, JBanesaUser jbu);

        void Edit(Apartment apartment);

        void Delete(Apartment apartment);

        void Save();
    }
}
