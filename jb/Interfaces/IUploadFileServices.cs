﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Interfaces
{
    public interface IUploadFileServices
    {
        string AddBuildingPhotos(long buildingId, List<IFormFile> files);
        List<string> AllBuildingPhotos(long buildingId);
        void DeleteAllBuildingPhotos(long buildingId);

        string GetOnePhotoOfBuilding(long id);
    }
}
