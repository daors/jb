﻿using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Interfaces
{
    public interface ICityRepository
    {
        IEnumerable<City> GetAll();
        City GetById(long cityId);
    }
}
