﻿using jb.Models;
using jb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Interfaces
{
    public interface IBuildingRepository
    {
        // needs to be replaced with the one that returns photos too
        Building GetById(long id);
                
        // needs to be replaced with the one that returns photos too
        Building GetByIdIncludingUserAndCity(long id);
                
        // needs to be replaced with the one that returns photos too       
        IEnumerable<Building> GetAll();
                
        // needs to be replaced with the one that returns photos too          
        IEnumerable<Building> GetAllIncludingUsers();
        
        // needs to be replaced with the one that returns photos too
        IEnumerable<Building> GetAllIncludingUsersAndCities();

        // needs to be replaced with the one that returns photos too
        IEnumerable<Building> GetUserPostedBuildings(string jBanesaUserName);
        void Delete(Building building);

        List<BuildingViewModel> GetAllIncludingUsersAndCitiesWithPhotos();

        //IEnumerable<Building> BuildingSearched(SearchClass model);
    }
}
