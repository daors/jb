﻿using jb.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Services
{
    public class UploadFilesService : IUploadFileServices
    {
        private readonly IHostingEnvironment hostingEnvironment;

        private readonly string pathToBuildings;

        public UploadFilesService(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
            pathToBuildings = hostingEnvironment.WebRootPath + "/UploadedFiles/Buildings/";
        }

        // Upload all Photos
        public string AddBuildingPhotos(long buildingId, List<IFormFile> files)
        {
            double size = 0;
            int maxSizeOnMB = 10;

            int photosCounter = 0;
            string directoryName = CreateDirectory(buildingId);
            foreach (var file in files)
            {

                string fileExtension = GetFileExtension(file.FileName);

                if (fileExtension.Equals("unsupported"))
                {
                    Console.WriteLine("Unsupported type of picture");
                    return "Found unsupported type, proccess terminated.";
                }

                string fileName = directoryName + "/[" + photosCounter++ + "]" + fileExtension;
                size += file.Length;


                using (FileStream fs = System.IO.File.Create(fileName))
                {
                    double sizeOnMB = size / 1024 / 1024;
                    if (!(sizeOnMB > maxSizeOnMB))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                    }
                    else
                    {
                        // If photo failed due to high size, decrement photo counter
                        // so the next photo wont be counted
                        // [0] > Ok, [1] > not ok, decrement to 0 so itll increment to 1 again
                        Console.WriteLine("WARN LOG: File with name" + fileName + " got ignored because of its big size [" + sizeOnMB + "]MB");
                        photosCounter--;
                    }

                }
            }
            string message = $"{files.Count} file(s) / {size} bytes uploaded succesfully";
            return message;
        }

        // Get all photos
        public List<string> AllBuildingPhotos(long id)
        {

            //photosPathThatWillBeSentToTheView
            List<string> photosPath = new List<string>();

            //Directory to read from
            string pathToRead = pathToBuildings + id;

            foreach (string filePath in Directory.EnumerateFiles(pathToRead,
                    "*",
                    SearchOption.TopDirectoryOnly
                ))
            {

                int from = filePath.IndexOf("UploadedFiles/Buildings");
                int to = filePath.Length;
                //Get only the needed part of filepath on the system, that will later be used in the view.
                //Excludes c:/... and starts from UploadedFiles/Buildings/{buildingId}/{photoId}.
                string substringedFilePath = "/" + filePath.Substring(from, to - from);

                photosPath.Add(substringedFilePath);
            }

            return photosPath;
        }


        public string GetOnePhotoOfBuilding(long id)
        {
            
            // CONTINUEEE HEEREEEEEEEE

            //Directory to read from
            string pathToRead = pathToBuildings + id;
            var filePath = Directory.GetFiles(pathToRead);
            
            var photoToReturn = filePath[0];
            int from = photoToReturn.IndexOf("UploadedFiles/Buildings");
            int to = filePath.Length;
            //Get only the needed part of filepath on the system, that will later be used in the view.
            //Excludes c:/... and starts from UploadedFiles/Buildings/{buildingId}/{photoId}.
            string substringedFilePath = "/" + photoToReturn.Substring(from, to - from);

            return photoToReturn;
        }


        public void DeleteAllBuildingPhotos(long buildingId)
        {
            DeleteDirectory(buildingId);
        }

        //Returns path of where is created C:\\..\\..\\Buildings\\70015
        private string CreateDirectory(long buildingId)
        {
            string pathToCreate = pathToBuildings + buildingId;
            DirectoryInfo di = Directory.CreateDirectory(pathToCreate);
            return di.FullName;

        }

        private string DeleteDirectory(long buildingId)
        {
            string pathToDelete = pathToBuildings + buildingId;
            bool withSubdirectories = true;
            // If directory does not exist, don't even try   
            if (Directory.Exists(pathToDelete))
            {
                Directory.Delete(pathToDelete, withSubdirectories);
                return "Directory succesfully deleted: " + pathToDelete;
            }
            else
            {
                return "Directory didnt even exist: " + pathToDelete;
            }
        }

        private string GetFileExtension(string fileName)
        {

            string[] fileExtensions = { ".jpg", ".jpeg", ".png", ".pjpeg", ".jfif", ".pjp" };

            foreach (string extension in fileExtensions)
            {
                if (fileName.EndsWith(extension))
                    return extension;
            }

            return "unsupported";
        }
    }
}
