﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using jb.Db;

namespace jb.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20191004150137_jbinit")]
    partial class jbinit
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.11-servicing-32099")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("jb.Models.Building", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AirConditionersNumber");

                    b.Property<int>("BalconiesNumber");

                    b.Property<int>("BathroomsNumber");

                    b.Property<int>("BedsNumber");

                    b.Property<int>("BuiltYear");

                    b.Property<long>("CityId");

                    b.Property<int>("Clicks");

                    b.Property<DateTime>("DatePosted");

                    b.Property<string>("Description");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<int>("ElevatorsNumber");

                    b.Property<int>("FloorsNumber");

                    b.Property<int>("GaragesNumber");

                    b.Property<bool>("HasBasement");

                    b.Property<bool>("HasParking");

                    b.Property<bool>("HasTermokos");

                    b.Property<bool>("IsFurnished");

                    b.Property<bool>("IsIsolated");

                    b.Property<string>("JBanesaUserName");

                    b.Property<string>("Neighborhood")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.Property<bool>("OnSale");

                    b.Property<int>("Price");

                    b.Property<int>("RoomsNumber");

                    b.Property<int>("SquareMeter");

                    b.Property<string>("Street")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("CityId");

                    b.ToTable("Buildings");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Building");
                });

            modelBuilder.Entity("jb.Models.City", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.HasKey("Id");

                    b.ToTable("Cities");
                });

            modelBuilder.Entity("jb.Models.Apartment", b =>
                {
                    b.HasBaseType("jb.Models.Building");

                    b.Property<int>("BedroomsNumber");

                    b.Property<int>("FloorNumber");

                    b.Property<int>("SwimmingPoolsNumber");

                    b.ToTable("Apartment");

                    b.HasDiscriminator().HasValue("Apartment");
                });

            modelBuilder.Entity("jb.Models.Building", b =>
                {
                    b.HasOne("jb.Models.City", "City")
                        .WithMany("Buildings")
                        .HasForeignKey("CityId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
