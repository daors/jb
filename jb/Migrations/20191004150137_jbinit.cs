﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace jb.Migrations
{
    public partial class jbinit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Buildings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 255, nullable: false),
                    Price = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Street = table.Column<string>(maxLength: 30, nullable: false),
                    SquareMeter = table.Column<int>(nullable: false),
                    BuiltYear = table.Column<int>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    OnSale = table.Column<bool>(nullable: false),
                    HasBasement = table.Column<bool>(nullable: false),
                    HasTermokos = table.Column<bool>(nullable: false),
                    HasParking = table.Column<bool>(nullable: false),
                    IsFurnished = table.Column<bool>(nullable: false),
                    IsIsolated = table.Column<bool>(nullable: false),
                    AirConditionersNumber = table.Column<int>(nullable: false),
                    ElevatorsNumber = table.Column<int>(nullable: false),
                    GaragesNumber = table.Column<int>(nullable: false),
                    RoomsNumber = table.Column<int>(nullable: false),
                    BathroomsNumber = table.Column<int>(nullable: false),
                    FloorsNumber = table.Column<int>(nullable: false),
                    BalconiesNumber = table.Column<int>(nullable: false),
                    BedsNumber = table.Column<int>(nullable: false),
                    DatePosted = table.Column<DateTime>(nullable: false),
                    Clicks = table.Column<int>(nullable: false),
                    CityId = table.Column<long>(nullable: false),
                    Neighborhood = table.Column<string>(maxLength: 30, nullable: false),
                    JBanesaUserName = table.Column<string>(nullable: true),
                    FloorNumber = table.Column<int>(nullable: true),
                    SwimmingPoolsNumber = table.Column<int>(nullable: true),
                    BedroomsNumber = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buildings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Buildings_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Buildings_CityId",
                table: "Buildings",
                column: "CityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Buildings");

            migrationBuilder.DropTable(
                name: "Cities");
        }
    }
}
