﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace jb.Migrations
{
    public partial class newfieldsbuildingcontroller : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasAlaram",
                table: "Buildings",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "HasSecurity",
                table: "Buildings",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "HasTV",
                table: "Buildings",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "HasWireless",
                table: "Buildings",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasAlaram",
                table: "Buildings");

            migrationBuilder.DropColumn(
                name: "HasSecurity",
                table: "Buildings");

            migrationBuilder.DropColumn(
                name: "HasTV",
                table: "Buildings");

            migrationBuilder.DropColumn(
                name: "HasWireless",
                table: "Buildings");
        }
    }
}
