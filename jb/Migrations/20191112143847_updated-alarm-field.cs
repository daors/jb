﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace jb.Migrations
{
    public partial class updatedalarmfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "HasAlaram",
                table: "Buildings",
                newName: "HasAlarm");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "HasAlarm",
                table: "Buildings",
                newName: "HasAlaram");
        }
    }
}
