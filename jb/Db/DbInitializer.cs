﻿using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Db
{
    public class DbInitializer
    {

        public static void Seed(AppDbContext context)
        {

            if (!context.Cities.Any())
            {
                context.AddRange(
                    
                    new City { Name="Prishtina", Buildings= new List<Building>()},
                    new City { Name="Ferizaj", Buildings= new List<Building>()},
                    new City { Name="Mitrovic", Buildings= new List<Building>()},
                    new City { Name="Gjilan", Buildings= new List<Building>()}
                    
                    );
            }

            context.SaveChanges();

        }
    }
}
