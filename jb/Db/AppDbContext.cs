﻿using jb.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Db
{
    // We need to extend to DbContext in order to set up datbase
    public class AppDbContext : IdentityDbContext<JBanesaUser>
    {
        
        public AppDbContext(DbContextOptions<AppDbContext> options) 
            : base(options)
        {

        }

        // Here we specify cardinalities between tables
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Building>()
                .HasOne(p => p.JBanesaUser)
                .WithMany(b => b.PostedBuildings);

            modelBuilder.Entity<MatchmakingBuilding>()
                .HasOne(p => p.JBanesaUser)
                .WithMany(b => b.MatchmakingBuildings);
        }


        // Here we add models so that EF Core will create them as tables in database
        public DbSet<Building> Buildings { get; set; }
        public DbSet<Apartment> Apartments { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<JBanesaUser> JBanesaUsers { get; set; }
    }
}
