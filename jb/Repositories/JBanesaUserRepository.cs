﻿using jb.Db;
using jb.Interfaces;
using jb.Models;
using jb.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Repositories
{
    public class JBanesaUserRepository : IJBanesaUserRepository
    {

        private readonly AppDbContext context;
        private readonly IUploadFileServices uploadFileServices;


        public JBanesaUserRepository(AppDbContext context, IUploadFileServices uploadFileServices)
        {
            this.context = context;
            this.uploadFileServices = uploadFileServices;
        }

        public JBanesaUser GetJBanesaUser(JBanesaUser jbu)
        {
            return context.JBanesaUsers.Find(jbu);
        }

        public JBanesaUser GetById(string id)
        {
            return context.JBanesaUsers
                .FirstOrDefault(jbu => jbu.Id.Equals(id));
        }

        public JBanesaUser GetByIdWithEverything(string id)
        {
            return context.JBanesaUsers
                .Include(jbu => jbu.PostedBuildings)
                .Include(jbu => jbu.FavouriteBuildings)
                .Include(jbu => jbu.MatchmakingBuildings)
                .FirstOrDefault(jbu => jbu.Id.Equals(id));
        }

        public JBanesaUser GetByUserName(string username)
        {
            try
            {
                return context.JBanesaUsers
                    .FirstOrDefault(jbu => jbu.UserName.Equals(username));
            }
            catch (ObjectDisposedException e)
            {
                Console.WriteLine("Caught: {0}", e.Message);
            }
            return null;
        }

        //geteverythingbyusername
        public JBanesaUser GetByUserNameWithEverything(string username)
        {
            return context.JBanesaUsers
                .Include(jbu => jbu.PostedBuildings)
                .Include(jbu => jbu.MatchmakingBuildings)
                .Include(jbu => jbu.FavouriteBuildings)
                .FirstOrDefault(jbu => jbu.UserName.Equals(username));
        }

        public IEnumerable<Building> GetPostedBuildingsOfAJBanesaUserByUsername(string username)
        {
            return context.JBanesaUsers
                .Include(jbu => jbu.PostedBuildings)
                .FirstOrDefault(jbu => jbu.UserName.Equals(username))
                ?.PostedBuildings;
        }

        public IEnumerable<Building> GetPostedBuildingsOfAJBanesaUserWithAddressByUsername(string username)
        {
            return context.JBanesaUsers
                .Include(jbu => jbu.PostedBuildings)
                    .ThenInclude(pb => pb.City)
                .FirstOrDefault(jbu => jbu.UserName.Equals(username))
                ?.PostedBuildings;
        }


        public IEnumerable<MatchmakingBuilding> GetMatchmakingBuildingsOfAJBanesaUserWithAddressByUsername(string username)
        {
            return context.JBanesaUsers
                .Include(jbu => jbu.MatchmakingBuildings)
                    .ThenInclude(pb => pb.City)
                .FirstOrDefault(jbu => jbu.UserName.Equals(username))
                ?.MatchmakingBuildings;
        }

        public IEnumerable<Building> GetFavouritedBuildingsOfAJBanesaUserByUsername(string username)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MatchmakingBuilding> GetMatchmakingBuildingsOfAJBanesaUserByUsername(string username)
        {
            return context.JBanesaUsers
                .Include(jbu => jbu.MatchmakingBuildings)
                .FirstOrDefault(jbu => jbu.UserName.Equals(username))
                ?.MatchmakingBuildings;
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        public List<BuildingViewModel> GetPostedBuildingsOfAJBanesaUserWithAddressByUsernameWithPhotos(string username)
        {
            List<BuildingViewModel> posted = new List<BuildingViewModel>();
            
            var postedBuildings = context.JBanesaUsers
                .Include(jbu => jbu.PostedBuildings)
                    .ThenInclude(pb => pb.City)
                .FirstOrDefault(jbu => jbu.UserName.Equals(username))
                ?.PostedBuildings;

            if (postedBuildings == null)
                return null;


            foreach(var building in postedBuildings)
            {
                posted.Add(new BuildingViewModel { Building = building, PhotoPath = uploadFileServices.AllBuildingPhotos(building.Id) });
            }

            return posted;
        }
    }
}
