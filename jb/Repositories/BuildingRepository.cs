﻿using jb.Db;
using jb.Interfaces;
using jb.Models;
using jb.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Repositories
{
    public class BuildingRepository : IBuildingRepository
    {
        private readonly AppDbContext context;
        private readonly IUploadFileServices uploadFileServices;

        public BuildingRepository(AppDbContext context, IUploadFileServices uploadFileServices)
        {
            this.context = context;
            this.uploadFileServices = uploadFileServices;
        }

        public Building GetById(long id)
        {
            return context.Buildings.FirstOrDefault(b => b.Id == id);
        }

        public Building GetByIdIncludingUserAndCity(long id)
        {
            return context.Buildings.Include(b => b.JBanesaUser).Include(b => b.City).FirstOrDefault(b => b.Id == id);
        }

        public IEnumerable<Building> GetAll()
        {
            return context.Buildings;
        }

        public IEnumerable<Building> GetAllIncludingUsers()
        {
            return context.Buildings.Include(b => b.JBanesaUser);
        }

        public IEnumerable<Building> GetUserPostedBuildings(string jBanesaUserName)
        {

            return context.Buildings.Include(b => b.JBanesaUser).ThenInclude(jb => jb.PostedBuildings).Where(b => b.JBanesaUserName.Equals(jBanesaUserName));
        }

        public void Delete(Building building)
        {
            context.Buildings.Remove(building);
            context.SaveChanges();
        }

        public IEnumerable<Building> GetAllIncludingUsersAndCities()
        {
            // added AsNoTracking() The AsNoTracking() turns off change tracking which improves the performance
            // The OrderBy() is required to support Paging (Skip() & Take()) in EF
            return context.Buildings.AsNoTracking().OrderBy(p => p.Price).Include(b => b.JBanesaUser).Include(b => b.City);
        }

        public List<BuildingViewModel> GetAllIncludingUsersAndCitiesWithPhotos()
        {          
            List<BuildingViewModel> buildingViewModels = new List<BuildingViewModel>();

            var allBuildings = context.Buildings.AsNoTracking().OrderBy(p => p.Price).Include(b => b.JBanesaUser).Include(b => b.City).ToList();

            foreach (var b in allBuildings)
            {
                buildingViewModels.Add(new BuildingViewModel { Building = b, PhotoPath = uploadFileServices.AllBuildingPhotos(b.Id) });
            }

            return buildingViewModels;
        }

        /// <summary>
        /// Need to be checked again, refactor it completely...
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /*
        public IEnumerable<Building> BuildingSearched(SearchClass model)
        {
            var squareMeter = model.MeterKatror; //int
            var bedsNumber = model.NrIShtreterve; //int
            var buildingType = model.LlojiINdertimit; // string
            var status = model.Statusi; //
            var bathroomNumbers = model.NrIBanjove;
            var location = model.Lokacioni;
            //var location = "Prishtine";
            var onSale = false;
            var descriminator = "";


            if (!string.IsNullOrEmpty(status))
            {
                if (status.Equals("Shitje"))
                {
                    onSale = true;
                }
                else
                {
                    onSale = false;
                }
            }


            //handle descriminator
            switch (buildingType)
            {
                case "Banes�":
                    descriminator = "Apartment";
                    break;
                case "Sht�pi":
                    descriminator = "House";
                    break;
                case "Lokal":
                    descriminator = "Local";
                    break;
                case "Villa":
                    descriminator = "Villa";
                    break;
                default:
                    Console.WriteLine("Default case"); // something is off :/ ( xD )
                    break;
            }


            // TODO: maybe find another impl later
            // if the user specifies the city and buildingType
            var query = GetAllIncludingUsersAndCities();

            if (query != null)
            {
                if (!string.IsNullOrEmpty(location))
                    query = query.Where(x => x.City.Name.Equals(location));
                if (!string.IsNullOrEmpty(buildingType))
                    query = query.Where(x => x.Discriminator.Equals(descriminator));
                if (model.NrIShtreterve.HasValue)
                    query = query.Where(x => x.BedsNumber == bedsNumber);
                if (model.MeterKatror.HasValue)
                    query = query.Where(x => x.SquareMeter == squareMeter);
                if (model.NrIBanjove.HasValue)
                    query = query.Where(x => x.BathroomsNumber == bathroomNumbers);
                if (onSale == true)
                {
                    query = query.Where(x => x.OnSale == onSale);
                }
                else
                {
                    query = query.Where(x => x.OnSale == onSale);
                }


            }
            return query;
        }*/
    }
}
