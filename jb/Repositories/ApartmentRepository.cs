﻿using jb.Db;
using jb.Interfaces;
using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using jb.ViewModels;

namespace jb.Repositories
{
    public class ApartmentRepository : IApartmentRepository
    {

        private readonly AppDbContext context;
        private readonly IUploadFileServices uploadFileServices;

        public ApartmentRepository(AppDbContext context, IUploadFileServices uploadFileServices)
        {
            this.context = context;
            this.uploadFileServices = uploadFileServices;
        }


        public void Add(Apartment apartment)
        {
            context.Apartments.Add(apartment);
            GetBanesaUserByName(apartment.JBanesaUserName).PostedBuildings.Add(apartment);
            Save();
        }

        public void Add(Apartment apartment, JBanesaUser jbu)
        {
            context.Apartments.Add(apartment);
            jbu.PostedBuildings.Add(apartment);
            Save();
        }

        public void Delete(Apartment apartment)
        {
            context.Apartments.Remove(apartment);
            Save();
        }

        public void Edit(Apartment newApartment)
        {

            var oldOne = GetByIdIncludingUser(newApartment.Id);

            oldOne.City = newApartment.City;
            oldOne.CityId = newApartment.CityId;
            oldOne.Clicks = newApartment.Clicks;
            oldOne.Description = newApartment.Description;
            oldOne.Discriminator = "Apartment";
            oldOne.Neighborhood = newApartment.Neighborhood;
            oldOne.Price = newApartment.Price;
            oldOne.Street = newApartment.Street;
            oldOne.Title = newApartment.Title;
            oldOne.BalconiesNumber = newApartment.BalconiesNumber;
            oldOne.BathroomsNumber = newApartment.BathroomsNumber;
            oldOne.BedroomsNumber = newApartment.BedroomsNumber;
            oldOne.BedsNumber = newApartment.BedsNumber;
            oldOne.BuiltYear = newApartment.BuiltYear;
            oldOne.DatePosted = oldOne.DatePosted;
            oldOne.ElevatorsNumber = newApartment.ElevatorsNumber;
            oldOne.FloorNumber = newApartment.FloorNumber;
            oldOne.FloorsNumber = newApartment.FloorsNumber;
            oldOne.GaragesNumber = newApartment.GaragesNumber;
            oldOne.HasBasement = newApartment.HasBasement;
            oldOne.HasParking = newApartment.HasParking;
            oldOne.HasTermokos = newApartment.HasTermokos;
            oldOne.IsFurnished = newApartment.IsFurnished;
            oldOne.IsIsolated = newApartment.IsIsolated;
            oldOne.OnSale = newApartment.OnSale;
            oldOne.RoomsNumber = newApartment.RoomsNumber;
            oldOne.SquareMeter = newApartment.SquareMeter;
            oldOne.AirConditionersNumber = newApartment.AirConditionersNumber;
            oldOne.SwimmingPoolsNumber = newApartment.SwimmingPoolsNumber;
            oldOne.JBanesaUserName = newApartment.JBanesaUserName;
            oldOne.JBanesaUser = newApartment.JBanesaUser;

            Save();
        }

        public IEnumerable<Apartment> GetAll()
        {
            return context.Apartments;
        }

        /// <summary>
        /// We are not going to use this one anymore since this one returns only apartments but with no photos
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Apartment> GetAllForRentIncludingUserAndCity()
        {
            return context.Apartments.Include(u => u.JBanesaUser).Include(c => c.City).Where(s => !s.OnSale);
        }


        public List<ApartmentViewModel> GetAllForRentIncludingUserAndCityWithPhotos()
        {
            List<ApartmentViewModel> apartmentViewModels = new List<ApartmentViewModel>();

            var allApartmentsForRent = context.Apartments.Include(u => u.JBanesaUser).Include(c => c.City).Where(s => !s.OnSale);

            foreach (var apartment in allApartmentsForRent)
            {
                apartmentViewModels.Add(new ApartmentViewModel { Apartment = apartment, PhotoPath = uploadFileServices.AllBuildingPhotos(apartment.Id) });
            }

            return apartmentViewModels;
        }

        /// <summary>
        /// We are not going to use this one anymore since this one returns only apartments but with no photos
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Apartment> GetAllForSaleIncludingUserAndCity()
        {
            return context.Apartments.Include(u => u.JBanesaUser).Include(c => c.City).Where(s => s.OnSale).ToList();
        }

        public List<ApartmentViewModel> GetAllForSaleIncludingUserAndCityWithPhotos()
        {
            List<ApartmentViewModel> apartmentViewModels = new List<ApartmentViewModel>();

            var allApartmentsForSale = context.Apartments.Include(u => u.JBanesaUser).Include(c => c.City).Where(s => s.OnSale).ToList();

            foreach (var apartment in allApartmentsForSale)
            {
                apartmentViewModels.Add(new ApartmentViewModel { Apartment = apartment, PhotoPath = uploadFileServices.AllBuildingPhotos(apartment.Id) });
            }

            return apartmentViewModels;
        }


        public IEnumerable<Apartment> GetAllIncludingCity()
        {
            return context.Apartments.Include(c => c.City).ToList();
        }

        public IEnumerable<Apartment> GetAllIncludingUserAndCity()
        {
            return context.Apartments.Include(a => a.JBanesaUser).Include(a => a.City).ToList();
        }


        /// <summary>
        /// We are not going to use this one anymore since this one returns only apartments but with no photos
        /// </summary>
        /// <returns></returns>
        public List<ApartmentViewModel> GetAllIncludingUserAndCityWithPhotos()
        {

            List<ApartmentViewModel> apartmentViewModels = new List<ApartmentViewModel>();

            var allApartments= context.Apartments.Include(a => a.JBanesaUser).Include(a => a.City).ToList();

            foreach (var apartment in allApartments)
            {
                apartmentViewModels.Add(new ApartmentViewModel { Apartment = apartment, PhotoPath = uploadFileServices.AllBuildingPhotos(apartment.Id) });
            }

            return apartmentViewModels;
        }

        public Apartment GetById(long id)
        {
            return context.Apartments.FirstOrDefault(a => a.Id == id);
        }

        public Apartment GetByIdIncludingUser(long id)
        {
            return context.Apartments.Include(ap => ap.JBanesaUser).FirstOrDefault(a => a.Id == id);
        }

        
        /// <summary>
        /// We are not going to use this anymore since this returns only one apartment without photos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Apartment GetByIdIncludingUserAndCity(long id)
        {
            return context.Apartments.Include(ap => ap.JBanesaUser).Include(ap => ap.City).FirstOrDefault(a => a.Id == id);
        }


        public ApartmentViewModel GetByIdIncludingUserAndCityWithPhotos(long id)
        {
            var apartment = context.Apartments.Include(user => user.JBanesaUser).Include(city => city.City).FirstOrDefault(apartmentId => apartmentId.Id == id);

            // No apartment with that Id exists
            if (apartment == null)
                return null;


            List<string> photoPaths = uploadFileServices.AllBuildingPhotos(apartment.Id);
            return new ApartmentViewModel { Apartment = apartment, PhotoPath = photoPaths };
        }

        public void Save()
        {
            context.SaveChanges();
        }


        public JBanesaUser GetBanesaUserByName(string userName)
        {
            return context.Users.FirstOrDefault(u => u.UserName.Equals(userName));
        }
    }
}
