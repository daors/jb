﻿using jb.Db;
using jb.Interfaces;
using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Repositories
{
    public class CityRepository : ICityRepository
    {
        private readonly AppDbContext context;

        public CityRepository(AppDbContext context)
        {
            this.context = context;   
        }

        public IEnumerable<City> GetAll()
        {
            return context.Cities.ToList();
        }

        public City GetById(long cityId)
        {
            return context.Cities.FirstOrDefault(c => c.Id == cityId);
        }
    }
}
