﻿using jb.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class ApartmentFilesViewModel
    {
        public Apartment Apartment { get; set; }
        [Required(ErrorMessage = "Ju lutem ngarkoni fotografite e baneses")]
        [MaxLength(10, ErrorMessage = "Nuk mund te ngarkoni me shume se 10 fotografi.")]
        public List<IFormFile> Files { get; set; }
        public IEnumerable<City> Cities { get; set; }
    }
}
