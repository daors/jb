﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [RegularExpression("^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Emri perdoruesit duhet te filloj me shkronja, mund te permban edhe numra.")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Emri perdoruesit duhet te jete i gjate 3-30 karaktere")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Emri te mos jete i zbrazet")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Emri duhet te jete i gjate 3-30 karaktere")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Emri duhet te permbaj vetem shkronja.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Mbiemri te mos jete i zbrazet")]
        [RegularExpression("^[a-zA-Z]*$", ErrorMessage = "Mbiemri duhet te permbaj vetem shkronja.")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Mbiemriperdoruesit duhet te jete i gjate 3-30 karaktere")]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required(ErrorMessage = "Duhet ta mbushni numrin e telefonit sepse ne rast se do te postonit ndertim, ai do te perdorej.")]
        [StringLength(30, MinimumLength = 8, ErrorMessage = "Numri telefonit duhet te jete me i gjate se 7 karaktere")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Fjalekalimi te mos jete i zbrazet")]
        [StringLength(30, MinimumLength = 6)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Konfirmimi i fjalekalimit te mos jete i zbrazet")]
        [StringLength(30, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Fjalekalimi nuk eshte i njejte, shkruani perseri fjalekalimin")]
        public string ConfirmPassword { get; set; }
    }
}
