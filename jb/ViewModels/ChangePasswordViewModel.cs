﻿using jb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class ChangePasswordViewModel
    {
        public JBanesaUser jBuser { get; set; }

        [Required(ErrorMessage = "Fjalekalimi nuk duhet te jete i zbrazet!")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "Fjalekalimi nuk duhet te jete i zbrazet!")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Konfirmimi i fjalekalimit nuk duhet te jete i zbrazet!")]
        [Compare("NewPassword", ErrorMessage = "Fjalekalimi nuk eshte i njejte, shkruani perseri fjalekalimin")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
