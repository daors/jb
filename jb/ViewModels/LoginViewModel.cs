﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Emri perdoruesit nuk duhet te jete i zbrazet!")]
        [RegularExpression("^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Emri perdoruesit duhet te filloj me shkronja, mund te permban edhe numra.")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Emri perdoruesit duhet te jete i gjate 3-30 karaktere")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Fjalekalimi nuk duhet te jete i zbrazet!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}
