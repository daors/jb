﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class AllApartmentsViewModel
    {
        public string Type { get; set; }
        public string BuildingsInfo { get; set; }
        public List<ApartmentViewModel> AllApartmentsWithPhoto { get; set; }
    }
}
