﻿using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class ApartmentViewModel
    {

        public Apartment Apartment { get; set; }

        public List<string> PhotoPath { get; set; }
    }
}
