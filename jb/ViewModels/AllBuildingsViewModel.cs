﻿using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class AllBuildingsViewModel
    {
        public string Type { get; set; }
        public string BuildingsInfo { get; set; }
        public List<BuildingViewModel> AllBuildingsWithPhoto { get; set; }
        public List<Building> AllBuildings{ get; set; }

    }
}
