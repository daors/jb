﻿using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class AllPostedBuildingViewModel
    {
        public string Message { get; set; }

        public List<BuildingViewModel> PostedBuildings { get; set; }

        public JBanesaUser JBanesaUser { get; set; }
    }
}
