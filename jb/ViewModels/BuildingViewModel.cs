﻿using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class BuildingViewModel
    {
        public Building Building { get; set; }

        public List<string> PhotoPath { get; set; }

        public List<Building> PostedBuildings { get; set; }


        public string Message { get; set; }
    }
}
