﻿using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class MatchmakingBuildingsViewModel
    {
        public IEnumerable<MatchmakingBuilding> MatchmakingBuildings { get; set; }
        public JBanesaUser JbanesaUser { get; set; }
    }
}
