﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class ResetPasswordViewModel
    {
        public string Token { get; set; }
        public string Email { get; set; }

        [Required(ErrorMessage = "Fjalekalimi nuk duhet te jete i zbrazet!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Konfirmimi i fjalekalimit nuk duhet te jete i zbrazet!")]
        [Compare("Password", ErrorMessage = "Konfirmimi fjalekalimit ndryshon prej fjalekalimit")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
