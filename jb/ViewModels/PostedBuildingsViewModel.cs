﻿using jb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class PostedBuildingsViewModel
    {
        public IEnumerable<Building> PostedBuildings { get; set; }
        public JBanesaUser JbanesaUser { get; set; }
    }
}
