﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.ViewModels
{
    public class EmailConfirmationViewModel
    {
        public string Message { get; set; }
    }
}
