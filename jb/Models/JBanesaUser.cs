﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Models
{
    public class JBanesaUser : IdentityUser
    {
        [Required(ErrorMessage = ("Emri nuk mund te jete i zbrazet"))]
        public string FirstName { get; set; }

        [Required(ErrorMessage = ("Mbiemri nuk mund te jete i zbrazet"))]
        public string LastName { get; set; }

        public bool MatchmakeMe { get; set; }

        public List<Building> PostedBuildings { get; set; }

        public List<Building> FavouriteBuildings { get; set; }

        public List<MatchmakingBuilding> MatchmakingBuildings { get; set; }
    }
}
