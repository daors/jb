﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Models
{
    public class Apartment : Building
    {
        [Required]
        [Range(-5, 100, ErrorMessage = "Numri i katit duhet te jete -5 deri 100")]
        public int FloorNumber { get; set; }
        [Required]
        [Range(0, 100, ErrorMessage = "Numri i pishinave duhet te jete 0-100")]
        public int SwimmingPoolsNumber { get; set; }
        [Required]
        [Range(0, 100, ErrorMessage = "Numri i dhomave te fjetjes duhet te jete 0-100")]
        public int BedroomsNumber { get; set; }
    }
}
