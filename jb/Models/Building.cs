﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Models
{
    public class Building
    {
        [Required]
        public long Id { get; set; }

        [Required(ErrorMessage = "Titulli nuk duhet te jete i zbrazet!")]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Titulli te mos jete me i gjate se 100 karaktere.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Cmimi nuk duhet te jete i zbrazet")]
        [Range(0, 999999999, ErrorMessage = "Cmimi nuk mund te jete negative.")]
        public int Price { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "Emri rruges nuk duhet te jete i zbrazet!")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "Emri rruges nuk duhet te permbaje me pake se 3 shkronja dhe me shume se 30!")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Meter katrore nuk duhet te jete i zbrazet!")]
        [Range(1, 999999999, ErrorMessage = "Meter katrore duhet te jete prej 1 deri 999999999")]
        public int SquareMeter { get; set; }

        public int BuiltYear { get; set; }

        public string Discriminator { get; set; }

        //        [Required]
        public bool OnSale { get; set; }

        //        [Required]
        public bool HasBasement { get; set; }

        //        [Required]
        public bool HasTermokos { get; set; }

        //        [Required]
        public bool HasParking { get; set; }

        //        [Required]
        public bool IsFurnished { get; set; }

        //        [Required]
        public bool IsIsolated { get; set; }


        public bool HasWireless { get; set; }

        public bool HasTV { get; set; }

        public bool HasAlarm { get; set; }

        public bool HasSecurity { get; set; }

        [Required]
        public int AirConditionersNumber { get; set; }

        [Required(ErrorMessage = "Numri i liftave duhet te jete i specifikuar 0-5!")]
        [Range(0, 5)]
        public int ElevatorsNumber { get; set; }

        [Required(ErrorMessage = "Numri i garazheve duhet te jete 0-5!")]
        [Range(0, 5)]
        public int GaragesNumber { get; set; }

        [Required(ErrorMessage = "Numri i dhomave duhet te jete 0-100!")]
        [Range(0, 100)]
        public int RoomsNumber { get; set; }

        [Required(ErrorMessage = "Numri i banjove duhet te jete 0-100!")]
        [Range(0, 100)]
        public int BathroomsNumber { get; set; }

        [Required(ErrorMessage = "Numri i kateve duhet te jete 0-100!")]
        [Range(0, 100)]
        public int FloorsNumber { get; set; }

        [Required(ErrorMessage = "Numri i balkoneve duhet te jete 0-100!")]
        [Range(0, 100)]
        public int BalconiesNumber { get; set; }

        [Required(ErrorMessage = "Numri i shtreteve duhet te jete 0-200!")]
        [Range(0, 200)]
        public int BedsNumber { get; set; }

        public DateTime DatePosted { get; set; }

        public int Clicks { get; set; }

        public City City { get; set; }

        [Required(ErrorMessage = "Duhet te zgjedhet qyteti.!")]
        public long CityId { get; set; }

        [Required(ErrorMessage = "Emri lagjes nuk duhet te jete i zbrazet!")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "Emri lagjes nuk duhet te permbaje me pake se 3 shkronja dhe me shume se 30!")]
        public string Neighborhood { get; set; }

        // it has to stay not mapped till we implement Identity
        [NotMapped]
        public JBanesaUser JBanesaUser { get; set; }

        public string JBanesaUserName { get; set; }


    }
}
