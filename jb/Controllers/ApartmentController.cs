﻿using jb.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using jb.Models;
using jb.ViewModels;
using Microsoft.AspNetCore.Http;

namespace jb.Controllers
{
    public class ApartmentController : Controller
    {
        private readonly UserManager<JBanesaUser> userManager;

        private readonly IApartmentRepository apartmentRepository;
        private readonly ICityRepository cityRepository;
        //private readonly INeighborhoodRepository neighborhoodRepository;

       // private readonly IMatchmakingApartmentRepository matchmakingApartmentRepository;
        private readonly IJBanesaUserRepository jBanesaUserRepository;
        private readonly IUploadFileServices uploadFilesService;


        public ApartmentController(UserManager<JBanesaUser> userManager,
                                   IApartmentRepository apartmentRepository,
                                   IJBanesaUserRepository jBanesaUserRepository,
                                   ICityRepository cityRepository,
                                   IUploadFileServices uploadFileServices)
        {
            this.userManager = userManager;
            this.apartmentRepository = apartmentRepository;
            this.jBanesaUserRepository = jBanesaUserRepository;
            this.cityRepository = cityRepository;
            this.uploadFilesService = uploadFileServices;
        }


        public IActionResult AddApartment()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Account");

            ApartmentFilesViewModel apartmentVm = new ApartmentFilesViewModel();
            apartmentVm.Cities = cityRepository.GetAll().OrderBy(n => n.Name).ToList(); // we get all ordered cities here

            return View(apartmentVm);
        }


        [HttpPost]
        public async Task<IActionResult> AddApartment(ApartmentFilesViewModel apartmentFilesViewModel)
        {

            Apartment apartment = apartmentFilesViewModel.Apartment;
            List<IFormFile> photos = apartmentFilesViewModel.Files;

            if (ModelState.IsValid)
            {
                JBanesaUser jbu = await userManager.FindByNameAsync(User.Identity.Name);
                Apartment a = new Apartment
                {
                    City = cityRepository.GetById(apartment.CityId),
                    CityId = apartment.CityId,
                    Clicks = 1,
                    Description = apartment.Description,
                    Discriminator = "Apartment",
                    Id = apartment.Id,
                    Price = apartment.Price,
                    Street = apartment.Street,
                    Title = apartment.Title,
                    BalconiesNumber = apartment.BalconiesNumber,
                    BathroomsNumber = apartment.BathroomsNumber,
                    BedroomsNumber = apartment.BedroomsNumber,
                    BedsNumber = apartment.BedsNumber,
                    BuiltYear = apartment.BuiltYear,
                    DatePosted = DateTime.Now,
                    ElevatorsNumber = apartment.ElevatorsNumber,
                    FloorNumber = apartment.FloorNumber,
                    FloorsNumber = apartment.FloorsNumber,
                    GaragesNumber = apartment.GaragesNumber,
                    HasBasement = apartment.HasBasement,
                    HasParking = apartment.HasParking,
                    IsFurnished = apartment.IsFurnished,
                    IsIsolated = apartment.IsIsolated,
                    OnSale = apartment.OnSale,
                    RoomsNumber = apartment.RoomsNumber,
                    SquareMeter = apartment.SquareMeter,
                    AirConditionersNumber = apartment.AirConditionersNumber,
                    SwimmingPoolsNumber = apartment.SwimmingPoolsNumber,
                    Neighborhood = apartment.Neighborhood,
                    JBanesaUserName = apartment.JBanesaUserName,
                    JBanesaUser = jbu,
                    HasAlarm = apartment.HasAlarm,
                    HasWireless = apartment.HasWireless,
                    HasTV = apartment.HasTV,
                    HasSecurity = apartment.HasSecurity
                };

                apartmentRepository.Add(a, jbu);
                uploadFilesService.AddBuildingPhotos(a.Id, photos);
              //StartMatchmaking(a);
                return RedirectToAction("Index", "Home");
            }

            ApartmentFilesViewModel ac = new ApartmentFilesViewModel();
            ac.Apartment = apartment;
            ac.Cities = cityRepository.GetAll();
            ac.Files = photos;

            return View(ac);
        }


        [HttpGet]
        public IActionResult ApartmentDetails(long Id)
        {

            var apartment = apartmentRepository.GetByIdIncludingUserAndCityWithPhotos(Id);

            if(apartment == null)
            {
                ViewBag.Error = $"Apartamenti me id {Id} nuk egziston";
                return View("ErrorPage");
            }

            return View(apartment);
        }


        public IActionResult EditApartment(long id)
        {
            var a = apartmentRepository.GetById(id);

            if (!a.JBanesaUserName.Equals(User.Identity.Name))
            {
                ViewBag.Error = "Nuk jeni te autorizuar per te edituar ndertimin";
                return View("ErrorPage");
            }                

            ApartmentFilesViewModel ac = new ApartmentFilesViewModel();
            ac.Apartment = a;
            ac.Cities = cityRepository.GetAll();
            return View(ac);
        }

        [HttpPost]
        public async Task<IActionResult> Update(Apartment apartment)
        {
            apartment.City = cityRepository.GetById(apartment.CityId);
            apartment.JBanesaUser = await userManager.FindByNameAsync(User.Identity.Name);

            if (ModelState.IsValid)
            {
                apartmentRepository.Edit(apartment);
            }
            else
            {
                return RedirectToAction("EditApartment", new { id = apartment.Id });
            }

            return RedirectToAction("Index", "Home");
        }

    }
}
