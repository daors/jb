﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using jb.ViewModels;
using jb.Interfaces;

namespace jb.Controllers
{
    public class HomeController : Controller
    {

        private readonly IBuildingRepository buildingRepository;
        private readonly IUploadFileServices uploadFileServices;

        public HomeController(IBuildingRepository buildingRepository, IUploadFileServices uploadFileServices)
        {
            this.buildingRepository = buildingRepository;
            this.uploadFileServices = uploadFileServices;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Title = "jBanesa index!";
            var allBuildings = buildingRepository.GetAllIncludingUsersAndCitiesWithPhotos().ToList();

            if (allBuildings.Count() == 0)
            {
                var allBuildingsVm = new AllBuildingsViewModel
                {
                    Type = "Building",
                    BuildingsInfo = "Nuk ka ndonje ndertim te postuar",
                    AllBuildingsWithPhoto = allBuildings
                };

                return View(allBuildingsVm);
            }
            else
            {
                var allBuildingsVm = new AllBuildingsViewModel
                {
                    Type = "Building",
                    BuildingsInfo = "Të gjitha ndërtimet me qera apo në shitje",
                    AllBuildingsWithPhoto = allBuildings
                };

                return View(allBuildingsVm);
            }
        }        
    }
}
