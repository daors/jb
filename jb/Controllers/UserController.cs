﻿using jb.Interfaces;
using jb.Models;
using jb.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Controllers
{
    public class UserController : Controller
    {
        private readonly SignInManager<JBanesaUser> signInManager;
        private readonly UserManager<JBanesaUser> userManager;
        private readonly IJBanesaUserRepository userRepo;

        public UserController(SignInManager<JBanesaUser> signInManager,
                              UserManager<JBanesaUser> userManager,
                              IJBanesaUserRepository userRepo)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.userRepo = userRepo;
        }


        #region Change User Profile
        public IActionResult Profile()
        {
            var jbUser = userRepo.GetByUserNameWithEverything(signInManager.UserManager.GetUserName(User));
            return View(jbUser);
        }

        [HttpPost]
        public IActionResult Profile(JBanesaUser user)
        {
            var jbUser = userRepo.GetByUserNameWithEverything(signInManager.UserManager.GetUserName(User));

            if (ModelState.IsValid)
            {
                jbUser.FirstName = user.FirstName;
                jbUser.PhoneNumber = user.PhoneNumber;
                userRepo.SaveChanges();
                return RedirectToAction("Profile", "User");
            }
            return View(jbUser);
        }
        #endregion

        #region Posted Buildings    
        public IActionResult PostedBuildings()
        {
            JBanesaUser jbu = userRepo.GetByUserNameWithEverything(signInManager.UserManager.GetUserName(User));
            List<BuildingViewModel>postedBuildings = userRepo.GetPostedBuildingsOfAJBanesaUserWithAddressByUsernameWithPhotos(signInManager.UserManager.GetUserName(User));
             
            if (postedBuildings.Count() == 0)
            {
                return View(new AllPostedBuildingViewModel { Message = "Ju nuk keni postuar asnje ndertim", JBanesaUser = jbu, PostedBuildings = null });
            }
            
            return View(new AllPostedBuildingViewModel { Message = "", JBanesaUser = jbu, PostedBuildings = postedBuildings});
        }

        #endregion

        #region Buildings that user wants
        public IActionResult MatchmakingBuildings()
        {
            JBanesaUser jbu = userRepo.GetByUserNameWithEverything(signInManager.UserManager.GetUserName(User));
            IEnumerable<MatchmakingBuilding> matchmakingBuildings = userRepo.
                GetMatchmakingBuildingsOfAJBanesaUserWithAddressByUsername(signInManager.UserManager.GetUserName(User));

            // Initialize view model so that we can set the values and send them to front end
            var matchmakingBuildingsViewModel = new MatchmakingBuildingsViewModel()
            {
                MatchmakingBuildings = matchmakingBuildings,
                JbanesaUser = jbu
            };

            return View(matchmakingBuildingsViewModel);
        }
        #endregion

        #region Change User Password
        [HttpGet]
        public IActionResult ChangePassword()
        {
            JBanesaUser jbu = userRepo.GetByUserNameWithEverything(signInManager.UserManager.GetUserName(User));
            var chpvm = new ChangePasswordViewModel()
            {
                jBuser = jbu
            };
            return View(chpvm);
        }

        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordViewModel model)
        {
            JBanesaUser jbu = userRepo.GetByUserNameWithEverything(signInManager.UserManager.GetUserName(User));

            var changePasswordVm = new ChangePasswordViewModel()
            {
                jBuser = jbu,
                CurrentPassword = model.CurrentPassword,
                NewPassword = model.NewPassword,
                ConfirmPassword = model.ConfirmPassword
            };

            if (ModelState.IsValid)
            {
                // check if user is null
                if (jbu != null)
                {
                    var result = userManager.ChangePasswordAsync(jbu, model.CurrentPassword, model.NewPassword);
                    if (result.Result.Succeeded)
                    {
                        signInManager.SignOutAsync(); // sign out from the current log in
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewData["wrongCurrentPassword"] = "Keni shenuar fjalekalimin aktual gabim!";
                        ModelState.AddModelError("", "Fjalekalimi aktual gabim");
                        return View(changePasswordVm);
                    }
                }
            }
            return View(changePasswordVm);
        }
        #endregion
    }
}
