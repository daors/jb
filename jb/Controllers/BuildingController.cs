﻿using jb.Interfaces;
using jb.Models;
using jb.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Controllers
{
    public class BuildingController : Controller
    {
        private readonly IBuildingRepository buildingRepository;
        private readonly IApartmentRepository apartmentRepository;
        private readonly IJBanesaUserRepository jBanesaUserRepository;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly UserManager<JBanesaUser> userManager;

        private readonly IUploadFileServices uploadFilesService;

        public BuildingController(IBuildingRepository buildingRepository,
                                    IApartmentRepository apartmentRepository,
                                    ICityRepository cityRepository,                                 
                                    IJBanesaUserRepository jBanesaUserRepository,
                                    IHostingEnvironment hostingEnvironment,
                                    UserManager<JBanesaUser> userManager,
                                    IUploadFileServices uploadFilesService)
        {
            this.buildingRepository = buildingRepository;
            this.apartmentRepository = apartmentRepository;
            this.jBanesaUserRepository = jBanesaUserRepository;
            this.hostingEnvironment = hostingEnvironment;
            this.userManager = userManager;
            this.uploadFilesService = uploadFilesService;
        }

        // ################################## THIS METHOD IS USED WHEN WE SEARCH FOR A BUILDING #####################################
        /*
        [HttpPost]
        public IActionResult AllBuildings(SearchClass model)
        {

            // maybe need to add validations
            var searchResult = buildingRepository.BuildingSearched(model);
            var obj = new AllBuildingsViewModel();
            if (searchResult != null && searchResult.Any())
            {

                obj.Type = model.LlojiINdertimit;
                obj.BuildingsInfo = "Ndertimet e kerkuara";
                obj.AllBuildings = searchResult;

                // then return the view with the queried buildings
                return View("AllBuildings", obj);
            }
            else
            {
                obj.BuildingsInfo = "Nuk ka ndertime me filtrimet e kerkuara";
                obj.Type = "";
                obj.AllBuildings = null;
                return View("AllBuildings", obj);
            }
         }*/


              
        [HttpGet]
        public IActionResult AllBuildings()
        {
            var allBuildings = buildingRepository.GetAllIncludingUsersAndCitiesWithPhotos();

            if (allBuildings == null)
            {
                var modeli = new AllBuildingsViewModel
                {
                    Type = "Building",
                    BuildingsInfo = "Nuk eshte postuar asnje ndertim",
                    AllBuildingsWithPhoto =  new List<BuildingViewModel>()
                };

                return View("AllBuildings", modeli);
            }
            else
            {
                var modeli = new AllBuildingsViewModel
                {
                    Type = "Building",
                    BuildingsInfo = "Të gjitha ndërtimet me qera apo në shitje",
                    AllBuildingsWithPhoto = allBuildings != null ? allBuildings : new List<BuildingViewModel>()
                };

                return View("AllBuildings", modeli);
            }
        }

        [HttpGet]
        public IActionResult AllBuildingsForRent()
        {

            var allBuildingsForRent = buildingRepository.GetAllIncludingUsersAndCitiesWithPhotos().Where(b => b.Building.OnSale == false).ToList();

            if (allBuildingsForRent == null)
            {
                var modeli = new AllBuildingsViewModel
                {
                    Type = "Building",
                    BuildingsInfo = "Nuk eshte postuar asnje ndertim me qera",
                    AllBuildingsWithPhoto = new List<BuildingViewModel>()
                };

                return View("AllBuildings", modeli);
            }
            else
            {
                var modeli = new AllBuildingsViewModel
                {
                    Type = "Building",
                    BuildingsInfo = "Të gjitha ndërtimet me qera apo në shitje",
                    AllBuildingsWithPhoto = allBuildingsForRent != null ? allBuildingsForRent : new List<BuildingViewModel>()
                };

                return View("AllBuildings", modeli);
            }
        }

        [HttpGet]
        public IActionResult AllBuildingsForSale()
        {
            var allBuildingsForSale = buildingRepository.GetAllIncludingUsersAndCitiesWithPhotos().Where(b => b.Building.OnSale == true).ToList();

            if (allBuildingsForSale == null)
            {
                var modeli = new AllBuildingsViewModel
                {
                    Type = "Building",
                    BuildingsInfo = "Nuk eshte postuar asnje ndertim me qera apo ne shitje",
                    AllBuildingsWithPhoto = new List<BuildingViewModel>()
                };

                return View("AllBuildings", modeli);
            }
            else
            {
                var modeli = new AllBuildingsViewModel
                {
                    Type = "Building",
                    BuildingsInfo = "Të gjitha ndërtimet me qera apo në shitje",
                    AllBuildingsWithPhoto = allBuildingsForSale != null ? allBuildingsForSale : new List<BuildingViewModel>()
                };

                return View("AllBuildings", modeli);
            }
        }

        [HttpGet]
        public IActionResult AllApartments()
        {
            var allApartment = apartmentRepository.GetAllIncludingUserAndCityWithPhotos();

            if (allApartment == null)
            {
                var allApartments = new AllApartmentsViewModel
                {
                    Type = "Apartment",
                    BuildingsInfo = "Nuk eshte postuar asnje banese me qera apo ne shitje",
                    AllApartmentsWithPhoto = new List<ApartmentViewModel>()
                };

                return View("AllBuildings", allApartments);
            }
            else
            {
                var allApartments = new AllApartmentsViewModel
                {
                    Type = "Apartment",
                    BuildingsInfo = "Të gjitha banesat me qera apo në shitje",
                    AllApartmentsWithPhoto = apartmentRepository.GetAllIncludingUserAndCityWithPhotos()
                };

                return View("AllBuildings", allApartments);
            }
        }

        [HttpGet]
        public IActionResult AllApartmentsForSale()
        {

            var apartmentsForSale = apartmentRepository.GetAllForSaleIncludingUserAndCityWithPhotos();

            if (apartmentsForSale == null)
            {
                var allApartmentsForSale = new AllApartmentsViewModel
                {
                    Type = "Apartment",
                    BuildingsInfo = "Nuk eshte postuar asnje banese ne shitje",
                    AllApartmentsWithPhoto = new List<ApartmentViewModel>()
                };

                return View(allApartmentsForSale);
            }
            else
            {
                var allApartmentsForSale = new AllApartmentsViewModel
                {
                    Type = "Apartment",
                    BuildingsInfo = "Të gjitha banesat në shitje",
                    AllApartmentsWithPhoto = apartmentRepository.GetAllForSaleIncludingUserAndCityWithPhotos()
                };

                return View(allApartmentsForSale);
            }
        }

        [HttpGet]
        public IActionResult AllApartmentsForRent()
        {
            var apartmentsForRent = apartmentRepository.GetAllForRentIncludingUserAndCityWithPhotos();

            if (apartmentsForRent == null)
            {
                var allApartmentsForRent = new AllApartmentsViewModel
                {
                    Type = "Apartment",
                    BuildingsInfo = "Nuk eshte postuar asnje banese me qera",
                    AllApartmentsWithPhoto = new List<ApartmentViewModel>()
                };

                return View(allApartmentsForRent);
            }
            else
            {
                var allApartmentsForRent = new AllApartmentsViewModel
                {
                    Type = "Apartment",
                    BuildingsInfo = "Të gjitha banesat me qera",
                    AllApartmentsWithPhoto = apartmentRepository.GetAllForRentIncludingUserAndCityWithPhotos()
                };

                return View(allApartmentsForRent);
            }
        }

        /*
        [HttpGet]
        public IActionResult AllVillas()
        {
            
            var allVillas = new AllBuildingsViewModel();
            allVillas.Type = "Villa";
            allVillas.AllBuildings = villasRepository.GetAll();
            
            return View("AllBuildings", allVillas);
            

            return null;
        }
        */


        public IActionResult BuildingDetails(long id)
        {
            var building = buildingRepository.GetById(id);

            if (building == null)
            {
                ViewBag.Error = $"Ndertimi me id {id} nuk egziston";
                return View("ErrorPage");
            }                                        

            switch (building.Discriminator)
            {
                case "Apartment":
                    return RedirectToAction("ApartmentDetails", "Apartment", new { id = building.Id });
                //                case "House":
                //                    return RedirectToAction("HomeDetails", "Home", new {id = building.Id});
                //                case "Villa":
                //                    return RedirectToAction("VillaDetails", "Villa", new {id = building.Id});
                //                case "Lokal":
                //                    return RedirectToAction("LokalDetails", "Lokal", new {id = building.Id});
                default:
                    ViewBag.Error = "Nuk dihet tipi i ndertimit!";
                    return View("ErrorPage");
            }
        }

        [HttpGet]
        public IActionResult DeleteBuilding(long id)
        {
            var b = buildingRepository.GetById(id);

            if (b != null)
            {
                if (User.Identity.Name.Equals(b.JBanesaUserName))
                {
                    buildingRepository.Delete(b);
                    uploadFilesService.DeleteAllBuildingPhotos(id);
                }
            }
            else
            {
                ViewBag.Error = $"Nuk eshte gjetur banesa me id {id}";
                return View("ErrorPage");
            }

            return RedirectToAction("PostedBuildings", "User");
        }

    }
}
