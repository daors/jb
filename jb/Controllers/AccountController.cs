﻿using jb.Interfaces;
using jb.Models;
using jb.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jb.Controllers
{
    public class AccountController : Controller
    {

        private readonly UserManager<JBanesaUser> userManager;
        private readonly SignInManager<JBanesaUser> signinManager;       
        private readonly IJBanesaUserRepository userRepo;
        private readonly IEmailSender emailSender;

        public AccountController(UserManager<JBanesaUser> userManager,
                                 SignInManager<JBanesaUser> signinManager,
                                 IJBanesaUserRepository userRepo,
                                 IEmailSender emailSender)
        {
            this.userManager = userManager;
            this.signinManager = signinManager;
            this.userRepo = userRepo;
            this.emailSender = emailSender;
        }

        #region Public methods

        public IActionResult Login(string returnUrl)
        {
            if (signinManager.IsSignedIn(User))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["wrongUserOrPassword"] = null;
            return View(
                new LoginViewModel
                {
                    ReturnUrl = returnUrl
                });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {

            if (!ModelState.IsValid)
            {
                return View(loginViewModel);
            }

            var user = await userManager.FindByNameAsync(loginViewModel.UserName);

            if (user != null && user.EmailConfirmed == false)
            {
                ViewData["EmailNotConfirmed"] = "Email-i nuk eshte konfirmuar";
                return View(loginViewModel);
            }

            if (user != null)
            {
                var result = await signinManager.PasswordSignInAsync(user, loginViewModel.Password, false, false);

                if (result.Succeeded)
                {
                    if (string.IsNullOrEmpty(loginViewModel.ReturnUrl))
                        return RedirectToAction("Index", "Home");

                    return Redirect(loginViewModel.ReturnUrl);
                }
            }
            ViewData["wrongUserOrPassword"] = "Emri perdoruesit apo fjalekalimi gabim";
            ModelState.AddModelError("", "Username/password not found.");

            return View(loginViewModel);
        }

        public IActionResult Register()
        {
            if (signinManager.IsSignedIn(User))
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
        {
            ViewBag.UserExists = false;
            if (ModelState.IsValid)
            {
                if (!registerViewModel.Password.Equals(registerViewModel.ConfirmPassword))
                {
                    return View(registerViewModel);
                }

                var user = await userManager.FindByNameAsync(registerViewModel.UserName);

                if (user == null)
                {
                    user = new JBanesaUser
                    {
                        Id = Guid.NewGuid().ToString(),
                        UserName = registerViewModel.UserName,
                        FirstName = registerViewModel.FirstName,
                        LastName = registerViewModel.LastName,
                        Email = registerViewModel.Email,
                        PhoneNumber = registerViewModel.PhoneNumber,
                        PostedBuildings = new List<Building>(),
                        MatchmakingBuildings = new List<MatchmakingBuilding>(),
                        FavouriteBuildings = new List<Building>()
                    };

                    var result = await userManager.CreateAsync(user, registerViewModel.Password);

                    if (result.Succeeded)
                    {
                        
                        var emailToken = await userManager.GenerateEmailConfirmationTokenAsync(user);
                        var confirmationEmailURL = Url.Action("ConfirmEmailAddress", "Account",
                            new { token = emailToken, email = user.Email }, Request.Scheme);

                        // string email, string subject, string htmlMessage
                        await emailSender.SendEmailAsync(user.Email, "Konfirmoni emailin", $"\n Ju lutem klikoni linkun me poshte per konfirmimin e emailit te juaj! \n{confirmationEmailURL}");

                        ViewData["ConfirmEmail"] = "Eshte derguar emaili per konfirmimin e account-it.";
                        return View();

                       // return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ViewBag.UserExists = true;
                    return View(registerViewModel);
                }
            }
            return View(registerViewModel);
        }


        public async Task<IActionResult> ConfirmEmailAddress(string token, string email)
        {
            var user = await userManager.FindByEmailAsync(email);

            if (user != null)
            {
                var result = await userManager.ConfirmEmailAsync(user, token);

                if (result.Succeeded)
                {
                    return View(new EmailConfirmationViewModel { Message = "Email-i eshte konfirmuar, mund te kqyeni ne website!" });
                }
            }
            return View(new EmailConfirmationViewModel { Message = $"Error - Ka deshtuar konfirmimi i emailit {email}" });
        }


        public IActionResult ForgotPassword()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel forgotPasswordViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByEmailAsync(forgotPasswordViewModel.Email);

                if (user != null)
                {
                    var token = await userManager.GeneratePasswordResetTokenAsync(user);
                    var resetPasswordURL = Url.Action("ResetPassword", "Account",
                        new { token = token, email = user.Email }, Request.Scheme);

                    await emailSender.SendEmailAsync(user.Email, "Forgot Password", $"Username i juaji eshte: {user.UserName} \nHape kete link per te krijuar nje password tjeter: {resetPasswordURL}");
                }
                else
                {
                    ViewData["UserDontExist"] = "Nuk egziston nje user me kete email";
                    return View(forgotPasswordViewModel);
                }
            }
            ViewData["EmailSent"] = "Eshte derguar emaili per ndryshimin e password-it. Shikoni inboxin ne email.";
            return View(forgotPasswordViewModel);
        }


        public IActionResult ResetPassword(string token, string email)
        {
            return View(new ResetPasswordViewModel { Token = token, Email = email});
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    var result = await userManager.ResetPasswordAsync(user, model.Token, model.Password);

                    if (result.Succeeded)
                    {
                        ViewData["Success"]= "Passwordi eshte ndryshuar!";
                        return View(model);
                    }

                    ViewData["Error"] = "Passwordi nuk eshte ndryshuar...";
                    return View(model);
                }
            }

            return View(model);
        }


        public IActionResult Logout()
        {
            signinManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        # endregion
    }
}
